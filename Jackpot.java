import java.util.Scanner;
public class Jackpot{
		public static int wins;
		
		public static void main(String[] args){
			Scanner scan = new Scanner(System.in);
			boolean playAgain = false;
			String ans= " ";
			
			do{
				playGame();
				System.out.println("Play Again? (Y/N)");
				ans = scan.nextLine();
				playAgain = ans.equalsIgnoreCase("y");
			}while(playAgain);
			
			
			String print = (wins==1) ? " time" : " times";
			
			System.out.println("Thank you for playing! You won " + wins + print);
			
		}
		
		public static void playGame(){
			System.out.println("Welcome to jackpot! have fun!");
			Board board = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
			
				while (!(gameOver)){
					System.out.println(board);
					 if(board.playATurn()){
						gameOver = true;
					}
					else{
						numOfTilesClosed+=1;
					} 
				}
				if(numOfTilesClosed >=7){
					System.out.println("Good job you won! jackpot!");
					wins++;
				}
				else{
					System.out.println("Oh no! ya lost! Do better next time");
				}
		} 
}